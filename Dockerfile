FROM python:3.7-slim

ADD requirements.txt /app/requirements.txt
RUN mkdir /app/ex

WORKDIR /app

RUN python3 -m pip install -r /app/requirements.txt --no-cache-dir

ADD export.py /app/export.py

RUN python3 -m compileall ./

VOLUME /app/ex

ENTRYPOINT ["python3", "export.py"]
