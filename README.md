# Paper Matter CLI export

Command line tool to automate documents export, useful for backups

## Requirements

- Python 3.7

## Install Python modules

    python3 -m pip install -r requirements.txt
    
## Create configuration file (config.ini)

Place `config.ini` in the same directory as the script or use `--config` to specify a location

```ini
[PM]
server = https://papermatter.app
client_id = XX

# path of an existing folder
dest_folder = /some/path
```

`client_id` has to be created in the admin panel (Django OAuth Toolkit):

 1. Once logged to admin go to **Django Oauth Toolkit** > **Applications** > **Add**
 1. (not mandatory) Set **User** with user id of the desired user (use the search button to recover id from email)
 1. Set **Redirect uris**: [YOUR_PM_INSTANCE]/oauth2/authorization_code/
 1. Set **Client type** as **Public**
 1. Set **Authorization grant type** as **Authorization code**
 1. Set **Name** with something meaningful (e.g. export-cli-my-instance)
 1. Click **SAVE** button

## Authentication
Before you can use the CLI, you have to authenticate against papermatter.app or your Paper Matter instance. If you
host your own instance, ensure that you have created an app in your admin panel and correctly reported the `client_id` in
the configuration file.

To begin authentication:

    python3 export.py auth

Follow instructions on the console. An authentication file `auth.json` will be created in the current working folder.

## Basic usage

    python3 export.py export
    
Duplicate documents in the same folder will be skipped (duplicate are document with same name and same content)

Documents with the same name but a different content will have a unique ID (PID) append to their names.

If you wish to export all documents, including the duplicate, use option `--unique-filename` (in this case all documents will have the PID append to their name).

## Usage with Docker

    docker run --rm -v /path/to/config.ini:/app/config.ini -v /path/to/dir/:/app/ex export-cli export
    
## CLI options

_You can also use `python export.py --help` to see the documentation bellow._

Usage: `export.py [OPTIONS] export`

### --config FILENAME
Path to the configuration file, default to `config.ini`.

### --unique-filename
Append document pid to filename making every filename unique (duplicate documents will be exported).

### --dest-dir DIRECTORY
Destination directory for the export (automatically created if doesn't exist).

## Sync limitations

If you export several times an account to the same folder, already exported folders/documents shouldn't be exported again.

However it comes with some limitations, described bellow. which may lead to duplicate exported files or deleted documents/folders to be still present in your export folder:
 - Documents renamed or moved on your Paper Matter account will be downloaded again on next export
 - Folders or documents deleted on your Paper Matter account wont be deleted in your export folder
 
 To avoid theses limitations do not use the sync feature by always exporting your documents to a new folder (if you wish to automate the export process, you can use `--dest-dir` to pass a new folder at each export).