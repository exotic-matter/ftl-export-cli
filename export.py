#  Copyright (c) 2020. Exotic Matter SAS. All rights reserved.
#  Licensed under the MIT License. See LICENSE in the project root for license information.

import concurrent
import configparser
import hashlib
import json
import os
from concurrent.futures.thread import ThreadPoolExecutor
from json.decoder import JSONDecodeError
from pathlib import Path
from typing import Union, Optional

import click
import dateutil.parser
import requests
from click import UsageError, Abort
from oauthlib.oauth2 import OAuth2Error
from pathvalidate import sanitize_filepath, sanitize_filename
from requests.adapters import HTTPAdapter
from requests_oauthlib import OAuth2Session
from urllib3 import Retry

executor = ThreadPoolExecutor()
futures_downloads = []
futures_get = []
documents_md5 = {}

FTL_SUPPORTED_DOCUMENTS_TYPES = {
    ".pdf": ["application/pdf"],
    ".txt": ["text/plain"],
    ".rtf": ["application/rtf", "text/rtf"],
    ".doc": ["application/msword"],
    ".xls": ["application/vnd.ms-excel", "application/excel"],
    ".ppt": ["application/vnd.ms-powerpoint", "application/mspowerpoint"],
    ".docx": [
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    ],
    ".xlsx": ["application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"],
    ".pptx": [
        "application/vnd.openxmlformats-officedocument.presentationml.presentation"
    ],
    ".odt": ["application/vnd.oasis.opendocument.text"],
    ".odp": ["application/vnd.oasis.opendocument.presentation"],
    ".ods": ["application/vnd.oasis.opendocument.spreadsheet"],
}

MIMETYPES_EXT_DICT = {}

for ext, mimes in FTL_SUPPORTED_DOCUMENTS_TYPES.items():
    _ext = ext.lower()

    for mime in mimes:
        MIMETYPES_EXT_DICT[mime.lower()] = _ext


@click.group()
@click.option(
    "--config",
    envvar="PM_EXPORT_CONFIG",
    default="config.ini",
    help="Path to the configuration file, default to `config.ini`.",
    type=click.File(),
)
@click.option(
    "--unique-filename",
    is_flag=True,
    help="Append document pid to filename making every filename unique (duplicate documents will be exported).",
)
@click.option(
    "--dest-dir",
    envvar="PM_EXPORT_DEST_DIR",
    help="Destination directory for the export (automatically created if doesn't exist).",
    type=click.Path(file_okay=False, dir_okay=True, writable=True, readable=True),
)
@click.pass_context
def cli(ctx, config, unique_filename, dest_dir):
    # ensure that ctx.obj exists and is a dict (in case `cli()` is called
    # by means other than the `if` block below)
    ctx.ensure_object(dict)
    config_parser = configparser.ConfigParser()
    config_parser.read_file(config)
    ctx.obj["CONFIG"] = config_parser
    ctx.obj["UNIQUE_FILENAME"] = unique_filename
    ctx.obj["DEST_DIR"] = dest_dir


@cli.command()
@click.pass_context
def auth(ctx):
    click.echo("Starting authentication with Paper Matter")
    config = ctx.obj["CONFIG"]
    host = config["PM"]["server"]
    client_id = config["PM"]["client_id"]

    # Prepare OAuth2 URL
    pm = OAuth2Session(
        client_id, scope=["read"], redirect_uri=f"{host}/oauth2/authorization_code/",
    )
    authorization_url, _ = pm.authorization_url(
        f"{host}/oauth2/authorize/", approval_prompt="auto"
    )
    click.echo(f"Please open the following URL in your browser: {authorization_url}")

    # Ask for authorization code
    authorization_code = click.prompt(
        "Please enter the validation code shown in your browser", type=str
    )

    # Get token from Paper Matter server
    try:
        token = pm.fetch_token(
            f"{host}/oauth2/token", code=authorization_code, include_client_id=True
        )

        with open("auth.json", "w", encoding="utf-8") as f:
            json.dump(token, f, ensure_ascii=False, indent=4)
        click.echo(click.style("Authentication successful", fg="green"))
    except OAuth2Error as e:
        click.echo(
            click.style(f"The server refused your authorization code: {e}", fg="red")
        )


@cli.command()
@click.pass_context
def export(ctx):
    click.echo(click.style("Starting export v0.1.0", fg="green"))
    config = ctx.obj["CONFIG"]["PM"]
    host = config["server"]
    client_id = config["client_id"]
    dest_folder = Path(ctx.obj["DEST_DIR"] or config["dest_folder"])
    skip_existing = not ctx.obj["UNIQUE_FILENAME"]

    try:
        with open("auth.json", "r", encoding="utf-8") as f:
            authentication = json.load(f)
    except FileNotFoundError:
        raise UsageError("Missing authentication file, please use:\n export.py auth")
    except JSONDecodeError:
        raise UsageError("Bad auth.json file format. Please retry:\n export.py auth")

    if "access_token" not in authentication:
        raise UsageError("Bad auth.json file format. Please retry:\n export.py auth")

    if not dest_folder.exists():
        dest_folder.mkdir()

    retry_strategy = Retry(
        total=3,
        backoff_factor=2,
        status_forcelist=[429, 500, 502, 503, 504],
        method_whitelist=["HEAD", "GET", "OPTIONS"],
    )
    adapter = HTTPAdapter(max_retries=retry_strategy)

    def token_updater(token):
        click.echo("Updated authentication file after refreshing token")
        with open("auth.json", "w", encoding="utf-8") as ff:
            json.dump(token, ff, ensure_ascii=False, indent=4)

    with OAuth2Session(
        client_id,
        token=authentication,
        auto_refresh_kwargs={"client_id": client_id},
        auto_refresh_url=f"{host}/oauth2/token",
        token_updater=token_updater,
    ) as s:
        s.mount("http://", adapter)
        s.mount("https://", adapter)

        # Small request to test credentials
        r = s.get(f"{host}/app/api/v1/documents?limit=1")

        if r.status_code == 200:
            root = sanitize_filepath(dest_folder, platform="auto")
            folders_count = make_folders(s, host, root, skip_existing=skip_existing)
            futures_downloads.append(
                executor.submit(
                    download_files, s, host, root, skip_existing=skip_existing
                )
            )

            documents_count = 0
            downloads_count = 0
            for f in concurrent.futures.as_completed(futures_downloads):
                documents_count += f.result()

            for f in concurrent.futures.as_completed(futures_get):
                if f.exception():
                    raise f.exception()

                downloads_count += 1

            executor.shutdown(wait=True)

            click.echo(click.style("Total", fg="green"))
            click.echo(
                f"Folders: {folders_count} / Documents: {documents_count} / Downloaded : {downloads_count}"
            )
        else:
            click.echo(
                f"FATAL: API Error {r.status_code} - {r.reason}, no export has been made"
            )
            raise Abort


def make_folders(
    s: requests.Session,
    host: str,
    current_folder: Path,
    folder: int = None,
    skip_existing: bool = False,
) -> int:
    if folder:
        r = s.get(f"{host}/app/api/v1/folders", params={"level": folder})
    else:
        r = s.get(f"{host}/app/api/v1/folders")

    if r.status_code == 200:
        folders_obj = r.json()
    else:
        click.echo(f"API Error {r.reason}")
        return 0

    count_f = 0

    for folder in folders_obj:
        folder_path = sanitize_filepath(
            current_folder / folder["name"], platform="auto"
        )
        try:
            folder_path.mkdir()
        except FileExistsError:
            click.echo(f"Folder {folder_path} already exists")
            pass

        count_f += 1

        futures_downloads.append(
            executor.submit(
                download_files,
                s,
                host,
                folder_path,
                folder=folder["id"],
                skip_existing=skip_existing,
            )
        )

        if folder["has_descendant"]:
            count_f += make_folders(
                s, host, folder_path, folder["id"], skip_existing=skip_existing
            )

    return count_f


def download_files(
    s: requests.Session,
    host: str,
    current_folder: Path,
    folder: int = None,
    skip_existing: bool = False,
) -> int:
    if folder:
        r = s.get(f"{host}/app/api/v1/documents", params={"level": folder})
    else:
        r = s.get(f"{host}/app/api/v1/documents")

    if r.status_code == 200:
        current_docs = r.json()
    else:
        click.echo(f"API Error {r.reason}")
        return 0

    count = 0

    while True:
        for doc in current_docs["results"]:
            download_file(
                s, f"{host}{doc['download_url']}", current_folder, doc, skip_existing,
            )
            count += 1

        if current_docs["next"] is None:
            break
        else:
            # click.echo(f"Next page {current_docs['next']}")
            r = s.get(current_docs["next"])
            if r.status_code == 200:
                current_docs = r.json()
            else:
                break

    return count


def download_file(
    s: requests.Session, url: str, folder: Path, doc: dict, skip_existing: bool = False
) -> Path:
    assert Path(folder).is_dir()
    pid = doc["pid"]
    md5 = doc["md5"] or None  # Some earlier documents have no md5

    local_filename = Path(
        sanitize_filename(doc["title"].strip(), max_len=200, platform="universal")
    )

    ext = doc["ext"] if "ext" in doc else mimetype_to_ext(doc["type"])

    if local_filename.suffix.lower() != ext:
        local_filename = f"{local_filename}{ext}"
    filepath = sanitize_filepath(folder / local_filename, platform="auto")
    current_filepath = filepath

    if not md5:
        # Can't risk skipping document with same name when we can't verify the md5, so we are making it unique
        current_filepath = sanitize_filepath(
            current_filepath.with_suffix(f".{pid}{current_filepath.suffix}"),
            platform="auto",
        )
        click.echo(f"No MD5 hash, making unique filename")

    if not skip_existing:
        current_filepath = sanitize_filepath(
            filepath.with_suffix(f".{pid}{filepath.suffix}"), platform="auto"
        )

    if current_filepath.exists():
        if md5 and md5_from_file(current_filepath) == md5:
            click.echo(f"Skipping already on disk {current_filepath}")
            return current_filepath
        else:
            # Handle case where document is not the same (binary wise) but share the same name
            current_filepath = sanitize_filepath(
                filepath.with_suffix(f".{pid}{filepath.suffix}"), platform="auto"
            )
            click.echo(f"Making unique filename {current_filepath} (content differ)")

            if current_filepath.exists():
                if md5 and md5_from_file(current_filepath) == md5:
                    click.echo(
                        f"Skipping already on disk after renaming {current_filepath}"
                    )
                    return current_filepath
                else:
                    current_filepath = sanitize_filepath(
                        filepath.with_suffix(f".{md5[0:6]}.{pid}{filepath.suffix}"),
                        platform="auto",
                    )
                    click.echo(
                        f"Error! Unique filename already exists but content is different! Renaming to {current_filepath}"
                    )

    filepath = current_filepath

    # Make the document exists as soon as possible so other download task can check if the file exists
    documents_md5[filepath] = md5
    filepath.touch()

    futures_get.append(executor.submit(get_remote_file, doc, filepath, md5, s, url))

    return filepath


def get_remote_file(
    doc, filepath: Union[str, Path], md5: str, s: requests.Session, url: str
):
    # Based on https://stackoverflow.com/a/39217788
    click.echo(f"Starting download {filepath}")
    with s.get(url, stream=True) as r:
        r.raise_for_status()

        with open(filepath, "wb") as f:
            for chunk in r.iter_content(chunk_size=1024):
                f.write(chunk)

        click.echo(f"Download finished {filepath}")

    # Set Access and Modification time for the file
    # FIXME set Creation time too (but more complex to do)
    date_created = dateutil.parser.isoparse(doc["created"])
    os.utime(filepath, (date_created.timestamp(), date_created.timestamp()))

    if md5 and md5_from_file(filepath) != md5:
        click.echo(f"Incorrect MD5 for {filepath}", err=True)


def md5_from_file(filename: Union[str, Path]) -> str:
    f = Path(filename)
    assert f.is_file()

    if f.stat().st_size == 0 and f in documents_md5:
        return documents_md5[f]
    else:
        hash_ = hashlib.md5()
        with open(filename, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_.update(chunk)
        return hash_.hexdigest()


def mimetype_to_ext(mime: str) -> Optional[str]:
    if mime.lower() not in MIMETYPES_EXT_DICT:
        return None  # File is not supported
    return MIMETYPES_EXT_DICT[mime]


if __name__ == "__main__":
    os.environ[
        "OAUTHLIB_INSECURE_TRANSPORT"
    ] = "1"  # Some PM instance might not use https (local, intranet)
    cli()
