#  Copyright (c) 2020. Exotic Matter SAS. All rights reserved.
#  Licensed under the MIT License. See LICENSE in the project root for license information.
import configparser
import json
import os
import shutil
import tempfile
import unittest
from pathlib import Path
from unittest import mock
from unittest.mock import MagicMock, patch, call, ANY, mock_open

import dateutil
from click.testing import CliRunner
from requests_oauthlib import OAuth2Session

import export as export_cls
from export import (
    executor,
    md5_from_file,
    get_remote_file,
    download_file,
    download_files,
    make_folders,
)


# Fake ConfigParser with test config
class FakeConfigParser(dict):
    def __init__(self, config_dict):
        super().__init__()
        self.update(config_dict)

    def read_file(self, config_file):
        pass


class TestExport(unittest.TestCase):
    DUMMY_HOST = "https://dummyhost.local"

    def test_md5_from_file(self):
        self.assertEqual(
            "2b0d9bcba3913d2d26b364630dab4c4b", md5_from_file(Path("test.pdf"))
        )

    @patch.object(os, "utime")
    @patch.object(dateutil.parser, "isoparse", wraps=dateutil.parser.isoparse)
    @patch.object(export_cls, "md5_from_file")
    def test_get_remote_file(self, mock_md5_from_file, mock_iso_parse, mock_os_utime):
        response = MagicMock()
        response.iter_content.return_value = iter(
            [str.encode("my string"), str.encode("my string"), str.encode("my string")]
        )

        request_session = MagicMock()
        # Mock context manager
        request_session.get.return_value.__enter__.return_value = response

        mock_md5_from_file.return_value = "2b0d9bcba3913d2d26b364630dab4c4b"

        filename = tempfile.NamedTemporaryFile().name
        get_remote_file(
            {"created": "2020-01-01"},
            filename,
            "2b0d9bcba3913d2d26b364630dab4c4b",
            request_session,
            "http",
        )

        response.raise_for_status.assert_called()
        response.iter_content.assert_called()
        mock_iso_parse.assert_called_with("2020-01-01")
        mock_os_utime.assert_called_with(filename, (ANY, ANY))
        mock_md5_from_file.assert_called_with(filename)

    @patch.object(export_cls, "futures_get")
    @patch.object(executor, "submit")
    def test_download_file(self, mock_executor_submit, mock_futures_get):
        request_session = MagicMock()
        folder = Path(tempfile.mkdtemp())
        doc = {
            "pid": "304k3r-34t43t-23e32e2d-xdvf",
            "md5": "2b0d9bcba3913d2d26b364630dab4c4b",
            "title": "my document",
            "ext": ".pdf",
            "type": "application/pdf",
        }
        mock_futures_get.return_value = []

        result = download_file(
            request_session, TestExport.DUMMY_HOST, folder, doc, False
        )

        # Tests
        expected_path = folder / Path("my document.304k3r-34t43t-23e32e2d-xdvf.pdf")
        mock_executor_submit.assert_called_with(
            get_remote_file,
            doc,
            expected_path,
            "2b0d9bcba3913d2d26b364630dab4c4b",
            request_session,
            TestExport.DUMMY_HOST,
        )
        self.assertEqual(result, expected_path)
        self.assertTrue(expected_path.exists())
        mock_futures_get.append.assert_called_with(mock_executor_submit())

    @patch.object(export_cls, "futures_get")
    @patch.object(executor, "submit")
    def test_download_file_skip_existing(self, mock_executor_submit, mock_futures_get):
        request_session = MagicMock()
        folder = Path(tempfile.mkdtemp())
        doc = {
            "pid": "304k3r-34t43t-23e32e2d-xdvf",
            "md5": "2b0d9bcba3913d2d26b364630dab4c4b",
            "title": "my document",
            "ext": ".pdf",
            "type": "application/pdf",
        }
        mock_futures_get.return_value = []

        result = download_file(
            request_session, TestExport.DUMMY_HOST, folder, doc, True
        )

        # Tests
        expected_path_filename = folder / Path("my document.pdf")
        mock_executor_submit.assert_called_with(
            get_remote_file,
            doc,
            expected_path_filename,
            "2b0d9bcba3913d2d26b364630dab4c4b",
            request_session,
            TestExport.DUMMY_HOST,
        )

        self.assertEqual(result, expected_path_filename)
        self.assertTrue(expected_path_filename.exists())
        mock_futures_get.append.assert_called_with(mock_executor_submit())

    @patch.object(export_cls, "futures_get")
    @patch.object(executor, "submit")
    def test_download_file_already_exist_with_same_md5(
        self, mock_executor_submit, mock_futures_get
    ):
        request_session = MagicMock()
        folder = Path(tempfile.mkdtemp())
        doc = {
            "pid": "304k3r-34t43t-23e32e2d-xdvf",
            "md5": "2b0d9bcba3913d2d26b364630dab4c4b",
            "title": "my document",
            "ext": ".pdf",
            "type": "application/pdf",
        }
        mock_futures_get.return_value = []
        shutil.copyfile(Path("test.pdf"), folder / Path("my document.pdf"))

        # File already exists and has the same md5 (faked in the test)
        result = download_file(
            request_session, TestExport.DUMMY_HOST, folder, doc, True
        )

        # Tests
        self.assertEqual(result, folder / Path("my document.pdf"))
        mock_executor_submit.assert_not_called()

    @patch.object(export_cls, "futures_get")
    @patch.object(executor, "submit")
    def test_download_file_same_name_but_not_md5(
        self, mock_executor_submit, mock_futures_get
    ):
        request_session = MagicMock()
        folder = Path(tempfile.mkdtemp())
        doc = {
            "pid": "304k3r-34t43t-23e32e2d-xdvf",
            "md5": "2b0d9bcbaXXXXXXXXXXb364630dab4c4b",
            "title": "my document",
            "ext": ".pdf",
            "type": "application/pdf",
        }
        mock_futures_get.return_value = []

        shutil.copyfile(Path("test.pdf"), folder / Path("my document.pdf"))

        # File already exists but doesn't have the same md5 (faked in the test)
        result = download_file(
            request_session, TestExport.DUMMY_HOST, folder, doc, True
        )

        # Tests
        expected_path_filename = folder / Path(
            "my document.304k3r-34t43t-23e32e2d-xdvf.pdf"
        )
        self.assertEqual(result, expected_path_filename)
        mock_executor_submit.assert_called_with(
            get_remote_file,
            doc,
            expected_path_filename,
            "2b0d9bcbaXXXXXXXXXXb364630dab4c4b",
            request_session,
            TestExport.DUMMY_HOST,
        )
        self.assertTrue(expected_path_filename.exists())
        mock_futures_get.append.assert_called_with(mock_executor_submit())

    @patch.object(export_cls, "futures_get")
    @patch.object(executor, "submit")
    def test_download_file_same_unique_name_but_not_md5(
        self, mock_executor_submit, mock_futures_get
    ):
        request_session = MagicMock()
        folder = Path(tempfile.mkdtemp())
        doc = {
            "pid": "304k3r-34t43t-23e32e2d-xdvf",
            "md5": "2b0d9bcbaAAAAAAAAb364630dab4c4b",
            "title": "my document",
            "ext": ".pdf",
            "type": "application/pdf",
        }
        mock_futures_get.return_value = []

        shutil.copyfile(Path("test.pdf"), folder / Path("my document.pdf"))
        shutil.copyfile(
            Path("test.pdf"),
            folder / Path("my document.304k3r-34t43t-23e32e2d-xdvf.pdf"),
        )

        # File already exists but doesn't have the same md5 (faked in the test)
        result = download_file(
            request_session, TestExport.DUMMY_HOST, folder, doc, True
        )

        # Tests
        expected_path_filename = folder / Path(
            "my document.2b0d9b.304k3r-34t43t-23e32e2d-xdvf.pdf"
        )
        self.assertEqual(result, expected_path_filename)
        mock_executor_submit.assert_called_with(
            get_remote_file,
            doc,
            expected_path_filename,
            "2b0d9bcbaAAAAAAAAb364630dab4c4b",
            request_session,
            TestExport.DUMMY_HOST,
        )
        self.assertTrue(expected_path_filename.exists())
        mock_futures_get.append.assert_called_with(mock_executor_submit())

    @patch.object(export_cls, "download_file")
    def test_download_files(self, mock_download_file):
        # Fake API response
        api_documents = {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [
                {
                    "pid": "87ed3f01-6ec3-4890-8b6b-aa52a03f0d90",
                    "title": "test document",
                    "note": "",
                    "created": "2020-06-04T10:26:07.403805Z",
                    "edited": "2020-06-04T10:26:13.410173Z",
                    "ftl_folder": 45,
                    "thumbnail_available": True,
                    "thumbnail_url": "https://dummydownload",
                    "download_url": "/app/uploads/87ed3f01-6ec3-4890-8b6b-aa52a03f0d90",
                    "is_processed": True,
                    "path": [
                        {"id": 4, "name": "Factures"},
                        {"id": 45, "name": "Fournisseurs"},
                    ],
                    "md5": "fb4a75da243d699f901583f2434f07b2",
                    "size": 518177,
                    "ocrized": True,
                    "type": "application/pdf",
                },
            ],
        }

        request_session_response_api_documents = MagicMock()
        request_session_response_api_documents.status_code = 200
        request_session_response_api_documents.json.return_value = api_documents

        request_session = MagicMock()
        request_session.get.return_value = request_session_response_api_documents
        folder = Path(tempfile.mkdtemp())

        result = download_files(
            request_session, TestExport.DUMMY_HOST, folder, None, False
        )

        # Tests
        self.assertEqual(1, result)
        request_session.get.assert_called_with(
            f"{TestExport.DUMMY_HOST}/app/api/v1/documents"
        )
        mock_download_file.assert_called_with(
            request_session,
            f"{TestExport.DUMMY_HOST}{api_documents['results'][0]['download_url']}",
            folder,
            api_documents["results"][0],
            False,
        )

    @patch.object(export_cls, "download_file")
    def test_download_files_pagination(self, mock_download_file):
        # Fake API response with pagination
        api_documents_1 = {
            "count": 2,
            "next": f"{TestExport.DUMMY_HOST}/app/api/v1/documents?page=2",
            "previous": None,
            "results": [
                {
                    "pid": "87ed3f01-6ec3-4890-8b6b-aa52a03f0d90",
                    "title": "test document",
                    "note": "",
                    "created": "2020-06-04T10:26:07.403805Z",
                    "edited": "2020-06-04T10:26:13.410173Z",
                    "ftl_folder": None,
                    "thumbnail_available": True,
                    "thumbnail_url": f"{TestExport.DUMMY_HOST}/thumb",
                    "download_url": "/app/uploads/87ed3f01-6ec3-4890-8b6b-aa52a03f0d90",
                    "is_processed": True,
                    "path": [],
                    "md5": "fb4a75da243d699f901583f2434f07b2",
                    "size": 518177,
                    "ocrized": True,
                    "type": "application/pdf",
                },
            ],
        }
        api_documents_2 = {
            "count": 2,
            "next": None,
            "previous": f"{TestExport.DUMMY_HOST}/app/api/v1/documents?page=1",
            "results": [
                {
                    "pid": "bad4d9e1-3e09-499e-8039-9789d78f73b0",
                    "title": "test document 2",
                    "note": "",
                    "created": "2020-05-18T06:45:52.086000Z",
                    "edited": "2020-05-22T16:44:20.467008Z",
                    "ftl_folder": None,
                    "thumbnail_available": True,
                    "thumbnail_url": f"{TestExport.DUMMY_HOST}/thumb",
                    "download_url": "/app/uploads/bad4d9e1-3e09-499e-8039-9789d78f73b0",
                    "is_processed": True,
                    "path": [],
                    "md5": "73f544a3532bd1ff23affd466227f167",
                    "size": 47202,
                    "ocrized": False,
                    "type": "application/pdf",
                },
            ],
        }

        request_session_response_api_documents_1 = MagicMock()
        request_session_response_api_documents_1.status_code = 200
        request_session_response_api_documents_1.json.return_value = api_documents_1

        request_session_response_api_documents_2 = MagicMock()
        request_session_response_api_documents_2.status_code = 200
        request_session_response_api_documents_2.json.return_value = api_documents_2

        request_session = MagicMock()
        # First call of get, get response 1, second call, get response 2
        request_session.get.side_effect = [
            request_session_response_api_documents_1,
            request_session_response_api_documents_2,
        ]
        folder = Path(tempfile.mkdtemp())

        result = download_files(
            request_session, TestExport.DUMMY_HOST, folder, None, False
        )

        # Tests
        self.assertEqual(2, result)

        # API was called two times (pagination) and with the URL given in API response
        asserting_calls_get = [
            call(f"{TestExport.DUMMY_HOST}/app/api/v1/documents"),
            call(f"{TestExport.DUMMY_HOST}/app/api/v1/documents?page=2"),
        ]
        request_session.get.assert_has_calls(asserting_calls_get)

        # download_files has been called two times for the two documents
        asserting_calls_download_files = [
            call(
                request_session,
                f"{TestExport.DUMMY_HOST}{api_documents_1['results'][0]['download_url']}",
                folder,
                api_documents_1["results"][0],
                False,
            ),
            call(
                request_session,
                f"{TestExport.DUMMY_HOST}{api_documents_2['results'][0]['download_url']}",
                folder,
                api_documents_2["results"][0],
                False,
            ),
        ]
        mock_download_file.assert_has_calls(asserting_calls_download_files)

    @patch.object(Path, "mkdir", autospec=True)
    @patch.object(export_cls, "futures_downloads")
    @patch.object(executor, "submit")
    def test_make_folders(
        self, mock_executor_submit, mock_futures_downloads, mock_mkdir
    ):
        # Fake API responses for folders
        api_folder_1 = [
            {
                "id": 3,
                "name": "Documents",
                "created": "2020-05-22T16:25:12.439804Z",
                "parent": None,
                "paths": [{"id": 3, "name": "Documents"}],
                "has_descendant": True,
            }
        ]
        api_folder_2 = [
            {
                "id": 4,
                "name": "Factures",
                "created": "2020-05-22T16:25:18.740425Z",
                "parent": 3,
                "paths": [
                    {"id": 3, "name": "Documents"},
                    {"id": 4, "name": "Factures"},
                ],
                "has_descendant": False,
            },
            {
                "id": 53,
                "name": "TEST",
                "created": "2020-06-05T15:37:37.792264Z",
                "parent": 3,
                "paths": [{"id": 3, "name": "Documents"}, {"id": 53, "name": "TEST"}],
                "has_descendant": False,
            },
        ]

        request_session_response_api_folder_1 = MagicMock()
        request_session_response_api_folder_1.status_code = 200
        request_session_response_api_folder_1.json.return_value = api_folder_1

        request_session_response_api_folder_2 = MagicMock()
        request_session_response_api_folder_2.status_code = 200
        request_session_response_api_folder_2.json.return_value = api_folder_2

        request_session = MagicMock()
        # 1st call gets response 1, 2nd gets response 2, etc.
        request_session.get.side_effect = [
            request_session_response_api_folder_1,
            request_session_response_api_folder_2,
        ]

        folder = Path(tempfile.mkdtemp())

        result = make_folders(
            request_session, TestExport.DUMMY_HOST, folder, None, False
        )

        # Tests
        self.assertEqual(3, result)

        asserting_calls_request_get = [
            call(f"{TestExport.DUMMY_HOST}/app/api/v1/folders"),
            call(f"{TestExport.DUMMY_HOST}/app/api/v1/folders", params={"level": 3}),
        ]
        request_session.get.assert_has_calls(asserting_calls_request_get)

        # To be able to assert call to mkdir(), we need autospec=True in decorator
        # https://stackoverflow.com/a/52106927
        asserting_calls_mkdir = [
            call(folder / Path("Documents")),
            call(folder / Path("Documents/Factures")),
            call(folder / Path("Documents/TEST")),
        ]
        mock_mkdir.assert_has_calls(asserting_calls_mkdir)

        asserting_calls_executor_submit = [
            call(
                download_files,
                request_session,
                TestExport.DUMMY_HOST,
                folder / Path("Documents"),
                folder=3,
                skip_existing=False,
            ),
            call(
                download_files,
                request_session,
                TestExport.DUMMY_HOST,
                folder / Path("Documents") / Path("Factures"),
                folder=4,
                skip_existing=False,
            ),
            call(
                download_files,
                request_session,
                TestExport.DUMMY_HOST,
                folder / Path("Documents") / Path("TEST"),
                folder=53,
                skip_existing=False,
            ),
        ]
        mock_executor_submit.assert_has_calls(asserting_calls_executor_submit)
        mock_futures_downloads.append.assert_called_with(mock_executor_submit())

    @patch.object(OAuth2Session, "fetch_token")
    @patch.object(OAuth2Session, "authorization_url")
    @patch.object(json, "dump")
    @patch("builtins.open", new_callable=mock_open)
    @patch.object(configparser, "ConfigParser")
    def test_auth(
        self,
        mock_config_parser,
        mock_file,
        mock_json_dump,
        mocked_oauth2_session_authorization_url,
        mocked_oauth2_session_fetch_token,
    ):
        mock_config_parser.return_value = FakeConfigParser(
            {
                "PM": {
                    "server": TestExport.DUMMY_HOST,
                    "client_id": "AAA",
                    "dest_folder": "/",
                }
            }
        )

        json_token = {
            "access_token": "q9MnCBVGxesDHlg3u40MjZkfMv8YKD",
            "expires_in": 36000,
            "token_type": "Bearer",
            "scope": "read",
            "refresh_token": "Zh3sm1n181eBIUMgy3oQ88gW2vKg8U",
        }

        mocked_oauth2_session_authorization_url.return_value = "", ""
        mocked_oauth2_session_fetch_token.return_value = json_token

        mock_file.read_data = json_token

        # Use Click special runner for test
        runner = CliRunner()
        result = runner.invoke(export_cls.cli, ["auth"], input="user-input-token\n")
        print(result.output)

        # Tests
        self.assertEqual(result.exit_code, 0)
        self.assertIsNone(result.exception)

        mocked_oauth2_session_authorization_url.assert_called_once_with(
            f"{TestExport.DUMMY_HOST}/oauth2/authorize/", approval_prompt="auto"
        )

        mocked_oauth2_session_fetch_token.assert_called_once_with(
            f"{TestExport.DUMMY_HOST}/oauth2/token",
            code="user-input-token",
            include_client_id=True,
        )

        mock_file.assert_called_once_with("auth.json", "w", encoding="utf-8")
        mock_json_dump.assert_called_once_with(
            json_token, mock.ANY, ensure_ascii=False, indent=4
        )

    @patch.object(export_cls, "futures_get")
    @patch.object(export_cls, "futures_downloads")
    @patch.object(export_cls, "make_folders")
    @patch.object(executor, "submit")
    @patch.object(json, "load")
    @patch("builtins.open", new_callable=mock_open)
    @patch.object(OAuth2Session, "get")
    @patch.object(configparser, "ConfigParser")
    def test_export(
        self,
        mock_config_parser,
        mock_requests_session_get,
        mock_file,
        mock_json_load,
        mock_executor_submit,
        mock_make_folders,
        mock_futures_downloads,
        mock_futures_get,
    ):
        folder = Path(tempfile.mkdtemp())

        mock_config_parser.return_value = FakeConfigParser(
            {
                "PM": {
                    "server": TestExport.DUMMY_HOST,
                    "client_id": "AAA",
                    "dest_folder": folder.absolute(),
                }
            }
        )

        mock_json_load.return_value = {
            "access_token": "9GDK6YUZzAdYWcitlgwW256G2CYpK8",
            "expires_in": 36000,
            "token_type": "Bearer",
            "scope": "read",
            "refresh_token": "Zh3sm1n181eBIUMgy3oQ88gW2vKg8U",
        }

        mock_requests_session_get_response = MagicMock()
        mock_requests_session_get_response.status_code = 200
        mock_requests_session_get.return_value = mock_requests_session_get_response

        # Use Click special runner for test
        runner = CliRunner()
        result = runner.invoke(export_cls.cli, ["export"])
        print(result.output)

        # Tests
        self.assertEqual(result.exit_code, 0)
        self.assertIsNone(result.exception)

        mock_file.assert_called_once_with("auth.json", "r", encoding="utf-8")
        mock_json_load.assert_called_once_with(mock.ANY)

        mock_requests_session_get.assert_called_with(
            f"{TestExport.DUMMY_HOST}/app/api/v1/documents?limit=1"
        )

        mock_make_folders.assert_called_with(
            mock.ANY, TestExport.DUMMY_HOST, folder, skip_existing=True
        )

        mock_executor_submit.assert_called_with(
            download_files,
            mock.ANY,
            TestExport.DUMMY_HOST,
            folder.absolute(),
            skip_existing=True,
        )

    @patch.object(json, "dump")
    @patch.object(json, "load")
    @patch("builtins.open", new_callable=mock_open)
    @patch.object(OAuth2Session, "get")
    @patch.object(configparser, "ConfigParser")
    def test_export_expired_refresh_token(
        self,
        mock_config_parser,
        mock_requests_session_get,
        mock_file,
        mock_json_load,
        mock_json_dump,
    ):
        folder = Path(tempfile.mkdtemp())

        mock_config_parser.return_value = FakeConfigParser(
            {
                "PM": {
                    "server": TestExport.DUMMY_HOST,
                    "client_id": "AAA",
                    "dest_folder": folder.absolute(),
                }
            }
        )

        mock_json_load.return_value = {
            "access_token": "9GDK6YUZzAdYWcitlgwW256G2CYpK8",
            "expires_in": 36000,
            "token_type": "Bearer",
            "scope": "read",
            "refresh_token": "Zh3sm1n181eBIUMgy3oQ88gW2vKg8U",
        }

        mock_requests_session_get_response = MagicMock()
        mock_requests_session_get_response.status_code = 401
        mock_requests_session_get_response.reason = "Invalid token"
        mock_requests_session_get.return_value = mock_requests_session_get_response

        # Use Click special runner for test
        runner = CliRunner()
        result = runner.invoke(export_cls.cli, ["export"])
        print(result.output)

        # Tests
        self.assertEqual(result.exit_code, 1)
        self.assertIsNotNone(result.exception)

        # Open method should have been called 1 time (minus ctx manager calls)
        # One time for reading the json.file
        self.assertEqual(
            mock_file.mock_calls,
            [
                call("auth.json", "r", encoding="utf-8"),
                call().__enter__(),
                call().__exit__(None, None, None),
            ],
        )

        # JSON auth file was loaded
        mock_json_load.assert_called_once_with(mock.ANY)

        # Testing the credential
        mock_requests_session_get.assert_called_with(
            f"{TestExport.DUMMY_HOST}/app/api/v1/documents?limit=1"
        )

        # No saved auth json file
        mock_json_dump.assert_not_called()


if __name__ == "__main__":
    unittest.main()
